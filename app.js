const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser');
const path = require('path');
const mysql = require('mysql');

const app = express();

// Variable de configuracion

process.env.PORT = process.env.PORT || 3000;

// Connection with DataBase

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'danben',
    password: 'danbencb08',
    database: 'website'
});

// Verificando si la coneccion se llevo con exito

connection.connect(function( error ){
    if(error) {
        console.log('Error of connection : ', err.stack);
        return;
    }
    console.log('Connection succefull ID: ', connection.threadId);
});

// MiddleWares

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'views/assets')));
app.set('view engine', 'pug');
app.use(require('./src/assets/routes'));

app.listen(process.env.PORT, function(){
    console.log(`El servidor esta corriendo en el puerto ${ process.env.PORT } `);
});

