const express = require('express');
const bcrypt = require('bcryptjs');
const bodyParser = require('body-parser');
const app = express();


const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'danben',
    password: 'danbencb08',
    database: 'website'
});



connection.connect(function( error ){
    if(error) {
        console.log('Error of connection : ', err.stack);
        return;
    }
    console.log('Connection succefull ID: ', connection.threadId);
});


// app.use(bodyParser.urlencoded({ extended: true }));
app.get('/', function(req, res){
    res.render('index', { title: 'Hey', message: 'Hello there!' });
});

app.get('/registrate',function(req, res){
    res.render('registrate.pug');
});
app.post('/registrate',( req, res ) => {

    let { user, email, password } = req.body;

    if( user === '' || email === '' || password ==='' ) {
        res.send('<script>alert("Vuelve a intentarlo"); window.location="/";</script>');
        res.end();
    }else {
        let salt = 10;
        password = bcrypt.hashSync(password, salt);
        console.log(password);
        let query = `INSERT INTO usuarios(user, email, password ) VALUES ( '${ user }' ,'${ email }', '${ password }')`;
        connection.query(query, ( error, rows, fields ) => {
            if( error ) throw error;
            res.redirect('/login');
        });
    }
});

app.get('/login', function(req, res) {
    res.render( 'login' );
});
app.post('/login', function(req, res) {
    let { email, password } = req.body;

    if( email === "" || password === "" ) {
        res.render('login',{ respuesta: 'Dejaste un campo en blaco' });
        res.end();
    }

    let query = ' SELECT * FROM usuarios WHERE email = ?';
    connection.query(query,[ email ], function(error, rows, field) {
        if(error){
            throw new Error('Ocuarrio un error en la consulta')
        }else {
            if(rows.length > 0){
                bcrypt.compare('danbencb08', rows[0].password)
                    .then(function(resp) {
                        if(resp){
                            let { id } = rows[0];
                            res.redirect(`profile?id=${ id }`);
                            // app.get('/profile', function(req, res) {
                            //     let { user } = rows[0];
                            //     res.render('profile', {
                            //         user
                            //     });
                            // });
                        }else {
                            res.render('login', { respuesta: 'No coinciden los datos' });
                        }
                    })
                    .catch(function(error) {
                        console.log('Ocurrio un error', error);
                    })
            }
        }
    })

});
app.get('/profile', function(req, res) {
    let id =req.query.id;
    let query = 'SELECT * FROM usuarios WHERE id = ?';

    connection.query(query, [ id ], function( error, rows ,fields ) {
        if(error) {
            throw new Error('Ocurrio un error en la consulta');
        }else {
            let { user, email } = rows[0];
            res.render('profile', { user });
        }
    });

    // res.render('profile');

});



module.exports = app;
